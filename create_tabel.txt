CREATE TABLE customers( id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) , email VARCHAR(255) , password VARCHAR(255) ); 
CREATE TABLE orders ( id INT PRIMARY KEY AUTO_INCREMENT, amount INT, customers_id INT, FOREIGN KEY (customers_id) REFERENCES customers(id))
